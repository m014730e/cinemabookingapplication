<%@include file="header.jsp" %>
<%@page import="beans.Admin" %>
<%
    Admin admin = (Admin) request.getAttribute("admin");
%>
<h2>Update Administrator Details</h2>
    <form action="Controller" method="post">
        <p>Title: 
            <input name="title"
                   type="text"
                   class ="btn"
                   value="<%= admin.getTitle()%>" required/>
        </p>
        <p>First Name: 
            <input name="first_name"
                   type="text"
                   class ="btn"
                   value="<%= admin.getFirstName()%>" required/>
        </p>
        <p>Last Name: 
            <input name="last_name"
                   type="text"
                   class ="btn"
                   value="<%= admin.getLastName() %>" required/>
        </p>
        <p>Password: 
            <input name="password" 
                   type="password"
                   class ="btn"
                   value="<%= admin.getPassword()%>" required/>
        </p>
    <input type="hidden" name="adminId" value="<%= admin.getAdminId() %>" >
    <input type="submit" name="command" class ="btn" value="Update admin details" >
    </form>
<%@include file="footer.jsp" %>

