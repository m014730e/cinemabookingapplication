<%@include file="header.jsp" %>
<%@page import="beans.Admin" %>
<%
    Admin admin = (Admin) request.getAttribute("admin");
%>
    <h2>Administrator Details</h2>
    <table border="1">
        <tr>
            <th>ID</th>
            <th>Name</th>
        </tr>
        <tr>
            <td><%= admin.getAdminId()%></td>
            <td><%= admin.getTitle() %> <%= admin.getFirstName()%> <%= admin.getLastName() %></td>
        </tr>
    </table>

    <p></p>
    <form action="Controller" method="post">
    <p>
        <input type="hidden" name="adminId" value="<%= admin.getAdminId()%>" >
        <input type="submit" name="command" class ="btn" value="View admin's cinema" >
        <input type="submit" name="command" class ="btn" value="View all users" >
    </p>
        <input type="hidden" name="adminId" value="<%= admin.getAdminId() %>" >
        <input type="submit" name="command" class ="btn" value="Update admin's account" >
    </form>
<%@include file="footer.jsp" %>

