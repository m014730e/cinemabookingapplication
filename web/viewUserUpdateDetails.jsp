<%@include file="header.jsp" %>
<%@page import="beans.User" %>
<%
    User user = (User) request.getAttribute("user");
%>
<h2>Update User Details</h2>
    <form action="Controller" method="post">
        <p>Title: 
            <input name="title"
                   type="text"
                   class ="btn"
                   value="<%= user.getTitle() %>" required/>
        </p>
        <p>First Name: 
            <input name="first_name"
                   type="text"
                   class ="btn"
                   value="<%= user.getFirstName() %>" required/>
        </p>
        <p>Last Name: 
            <input name="last_name"
                   type="text"
                   class ="btn"
                   value="<%= user.getLastName() %>" required/>
        </p>
        <p>Password: 
            <input name="password" 
                   type="password"
                   class ="btn"
                   value="<%= user.getPassword() %>" required/>
        </p>
    <input type="hidden" name="userId" value="<%= user.getUserId()%>" >
    <input type="submit" name="command" class ="btn" value="Update" >
    </form>
<%@include file="footer.jsp" %>

