<%@page import="beans.Cinema"%>
<%@page import="beans.Showing"%>
<%@include file="header.jsp" %>
<%@page import="java.util.ArrayList"%>
<%
    ArrayList<Showing> showingsList = (ArrayList<Showing>) request.getSession().getAttribute("showingsList");
    Cinema cinema = (Cinema) request.getSession().getAttribute("adminCinema");
    String adminId = (String)request.getSession().getAttribute("adminId");
    String filmTitle = showingsList.get(0).getFilm_title();
%>
  <h2>Showing has been deleted!</h2>
    <form action="Controller" method="post">
        <input type="hidden" name="filmTitle" value="<%= filmTitle %>" >
        <input type="submit" name="command" class ="btn" value="View showings for admin">
        <input type="hidden" name="cinemaId" value="<%= cinema.getCinemaId() %>" >
        <input type="submit" name="command" class ="btn" value="View films for admin">
        <input type="hidden" name="adminId" value="<%= adminId %>" >
        <input type="submit" name = "command" class ="btn" value="View admin's account details" >
    </form>
        
<%@include file="footer.jsp" %>
