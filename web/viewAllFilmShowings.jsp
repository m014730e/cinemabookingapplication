<%@page import="beans.Showing"%>
<%@include file="header.jsp" %>
<%@page import="converters.DateConverter" %>
<%@page import="converters.TimeConverter" %>
<%@page import="java.util.ArrayList"%>
<%
    ArrayList<Showing> showings = (ArrayList<Showing>) request.getSession().getAttribute("showingsList");
    String userId = (String)session.getAttribute("userId");
    DateConverter dateConverter = new DateConverter();
    TimeConverter timeConverter = new TimeConverter();
%>
<h2>All Film Showings</h2>
    <div class = "left">
        <table border="1">
            <tr>
                <th>Film title</th>
                <th>Date</th>
                <th>Start Time</th>
                <th>End Time</th>
                <th>Seats Available</th>
                <th>Screen Number</th>
                <th>Book</th>
            <%
                for (Showing showing : showings)
                {
            %>
            <tr>
                <td><%= showing.getFilm_title()%></td>
                <td><%= dateConverter.getAsString(request,showing.getShowing_date()) %></td>
                <td><%= timeConverter.getAsString(request,showing.getStart_time()) %></td>
                <td><%= timeConverter.getAsString(request,showing.getEnd_time()) %></td>
                <%
                    if(showing.getNumber_of_seats()<= 0)
                    {
                %>
                <td>0</td>
                <%
                    }
                    else
                    {
                %>
                <td><%= showing.getNumber_of_seats() %></td>
                <%
                    }
                %>
                <td><%= showing.getScreen_num() %></td>
                
                <%
                    if(showing.getNumber_of_seats() > 0)
                    {
                %>
                <td><form action="Controller" method="post">
                        <input type ="submit" class ="btn" name ="command" value="Book">
                        <input type ="hidden" name ="userId" value="<%=userId %>">
                        <input type ="hidden" name ="showingId" value="<%=showing.getShowing_Id() %>">
                    </form>
                </td>
                <%
                    }
                    else
                    {
                %>
                <td><button type="button" class = "btn" disabled>No seats!</button></td>
                <%
                    }
                %>
            </tr>
        
            <%
                }
            %>
        </table>
            <form action="Controller" method="post">
                <input type="hidden" name="userId" value="<%= userId %>" >
                <input type="submit" name = "command" class ="btn" value="View account details" >
            </form>
            <input type="button" class="btn" value="Back" onclick="javascript:history.go(-1)">
                       
    </div>
    <div class = "right">
        <%
        if (showings.get(0).getFilm_title().equalsIgnoreCase("Star Wars"))
                {
        %> 
        <img class="displayed" src="resources/images/star_wars.jpg" alt="Star Wars image" style="width:400px;height:350px;">
        <%
                }
        if(showings.get(0).getFilm_title().equalsIgnoreCase("The hateful eight"))
                {
        %> 
        <img class="displayed" src="resources/images/the_hateful_eight.jpg" alt="The hateful eight image" style="width:400px;height:350px;">
        <%
                    }
        if(showings.get(0).getFilm_title().equalsIgnoreCase("Carol"))
                {
        %> 
        <img class="displayed" src="resources/images/carol.jpg" alt="Carol image" style="width:400px;height:350px;">
        <%
                    }
        if(showings.get(0).getFilm_title().equalsIgnoreCase("Joy"))
                {
        %> 
        <img class="displayed" src="resources/images/joy.jpg" alt="Joy image" style="width:400px;height:350px;">
        <%
                    }
        %>
    </div>
  </body>
</html>
        
