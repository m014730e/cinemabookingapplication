<%@include file="header.jsp" %>
<%@page import="java.util.ArrayList"%>
<%@page import="beans.User" %>
<%
    ArrayList<User> users = (ArrayList<User>) request.getAttribute("usersList");
%>
<h2>Users Details</h2>
    <table border="1">
        <tr>
            <th>User Id</th>
            <th>Name</th>
        </tr>
        <%
            for (User user : users)
            {
        %>
        <tr>
            <td><%= user.getUserId()%></td>
            <td><%= user.getTitle() %> <%= user.getFirstName()%> <%= user.getLastName() %></td>
        </tr>

        <%
            }
        %>
    </table>
<%@include file="footer.jsp" %>

