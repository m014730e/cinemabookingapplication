<%@page import="beans.Booking"%>
<%@include file="header.jsp" %>
<%@page import="converters.DateConverter" %>
<%@page import="converters.TimeConverter" %>
<%
    Booking booking = (Booking) request.getAttribute("booking");
    DateConverter dateConverter = new DateConverter();
    TimeConverter timeConverter = new TimeConverter();
%>
<h2>Booking Details</h2>
    <table border="1">
        <tr>
            <th>Booking Id</th>
            <th>Number of seats</th>
            <th>Film Title</th>
            <th>Date</th>
            <th>Start Time</th>
            <th>End Time</th>
            <th>Screen Number</th>
            <th>Cinema</th>
        </tr>
        <tr>
            <td><%= booking.getBookingId()%></td>
            <td><%= booking.getNumberOfSeats()%></td>
            <td><%= booking.getFilm_title() %></td>
            <td><%= dateConverter.getAsString(request,booking.getShowing_date()) %></td>
            <td><%= timeConverter.getAsString(request,booking.getStart_time()) %></td>
            <td><%= timeConverter.getAsString(request,booking.getEnd_time()) %></td>
            <td><%= booking.getScreen_number() %></td>
            <td><%= booking.getCinema_name()  %></td>
        </tr>
    </table>
    <form action="Controller" method="post">
                    <input type="hidden" name="bookingId" value="<%= booking.getBookingId() %>" >
                    <input type="submit" name = "command" class ="btn" value="Cancel booking" >
    </form>
    
<%@include file="footer.jsp" %>
