<%@page import="beans.Cinema"%>
<%@include file="header.jsp" %>
<%
    Cinema cinema = (Cinema) request.getSession().getAttribute("adminCinema");
%>
  <h2>Administrator's Cinema Theatre</h2>
    <h3>Cinema Details</h3>
    <table border="1">
        <tr>
            <th>Cinema name</th>
            <th>View Cinema Films</th>
        </tr>
        <tr>
            <td><%= cinema.getName()%></td>
            <td><form action="Controller" method="post">
                <input type="hidden" name="cinemaId" value="<%= cinema.getCinemaId()%>" >
                <input type="submit" name="command" class ="btn" value="View films for admin" >
                </form>
            </td>
        </tr>
    </table>
<%@include file="footer.jsp" %>
