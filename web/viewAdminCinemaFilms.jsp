<%@page import="beans.Showing"%>
<%@include file="header.jsp" %>
<%@page import="java.util.ArrayList"%>
<%
    ArrayList<Showing> showings = (ArrayList<Showing>) request.getAttribute("filmsList");
    if(showings.size() <= 0)
    {
%>

<h2>No films!</h2>
<%
    }
    else
    {
%>
<h2>Admin Cinema Films</h2>
    <h3>Films</h3>
    <table border="1">
        <tr>
            <th>Film title</th>
            <th>View showings</th>
        </tr>
        <%
            for (Showing showing : showings)
            {
        %>
        <tr>
            <td><%= showing.getFilm_title()%></td>
            <td><form action="Controller" method="post">
                <input type="hidden" name="filmTitle" value="<%= showing.getFilm_title() %>" >
                <input type="submit" name="command" class ="btn" value="View showings for admin" >
                </form>
            </td>
        </tr>

        <%
            }
        %>
        </table>
<%
    }
%>
<%@include file="footer.jsp" %>
