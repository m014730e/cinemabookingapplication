<%@page import="beans.User" %>
<%@include file="header.jsp" %>
<jsp:useBean id="user" class="User" scope="session" />

<h2>User registration</h2>

<form action="Controller" method="post">
    <p>Username: 
        <input name="userId"
               class ="btn"
               type="text" required />
    </p>
    <p>Title: 
        <input name="title"
               class ="btn"
               type="text" required/>
    </p>
    <p>First Name: 
        <input name="first_name"
               class ="btn"
               type="text" required />
    </p>
    <p>Last Name: 
        <input name="last_name"
               class ="btn"
               type="text" required />
    </p>
    <p>Password: 
        <input name="password" 
               class ="btn"
               type="password" required/>
    </p>
    
    <p>
        <input type="submit" class ="btn" name="command" value="Register" />
        <input type="reset" class ="btn" value="Reset" />
</p>
</form>
    <%@include file="footer.jsp" %>