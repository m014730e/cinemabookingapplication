<%@page import="beans.Booking"%>
<%@include file="header.jsp" %>
<%@page import="java.util.ArrayList"%>
<%
    ArrayList<Booking> bookings = (ArrayList<Booking>) request.getAttribute("bookingsList");
    String userId = (String) request.getSession().getAttribute("userId");
%>
    <h2>All Bookings Details</h2>
<%
    if(bookings.size() <= 0)
    {
%>
    <h3> You have no bookings!</h3> 
<%
    }
    else
    {
%>  
<h3>Bookings</h3>
    <table border="1">
        <tr>
            <th>Booking Id</th>
            <th>Number of seats</th>
            <th>Booking Details</th>
        </tr>
<%
            for (Booking booking : bookings)
            {
%>
        <tr>
            <td><%= booking.getBookingId()%></td>
            <td><%= booking.getNumberOfSeats()%></td>
            <td><form action="Controller" method="post">
                    <input type="hidden" name="bookingId" value="<%= booking.getBookingId() %>" >
                    <input type="submit" name = "command" class ="btn" value="View booking" >
                </form>
            </td>
        </tr>
<%
            }
%>
    </table>
<%
        }
%>           
    <form action="Controller" method="post">
        <input type="hidden" name="userId" value="<%= userId %>" >
        <input type="submit" name = "command" class ="btn" value="View account details" >
    </form>
<%@include file="footer.jsp" %>
