<%@page import="beans.User" %>
<%@page import="beans.Admin" %>
<%@include file="header.jsp" %>
<%
    if (request.getSession().getAttribute("user") == null)
    {
        request.getSession().setAttribute("user", new User());
    }

    if (request.getSession().getAttribute("admin") == null)
    {
        request.getSession().setAttribute("admin", new Admin());
    }
%>
    <h2>Log in</h2>
    <form action="" method="post">
        <h3>Please log in:</h3>
        <ul>
            <li><a href="userLogin.jsp">User log-in</a></li>
            <li><a href="adminLogin.jsp">Admin log-in</a></li>
        </ul>
    </form>

    <form action="" method="post">
           <h3>Or Register:</h3>
           <a href="register.jsp">Go to that link to register your account</a>
    </form>
  </body>
</html>
