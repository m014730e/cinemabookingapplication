<%@page import="beans.Cinema"%>
<%@include file="header.jsp" %>
<%@page import="java.util.ArrayList"%>
<%
    ArrayList<Cinema> cinemas = (ArrayList<Cinema>) request.getAttribute("cinemasList");
%>
<h2>All Cinema Theatres</h2>
    <h3>Cinema names</h3>
    <table border="1">
        <tr>
            <th>Cinema name</th>
            <th>View Cinema Films</th>
        </tr>
        <%
            for (Cinema cinema : cinemas)
            {
        %>
        <tr>
            <td><%= cinema.getName()%></td>
            <td><form action="Controller" method="post">
                <input type="hidden" name="cinemaId" value="<%= cinema.getCinemaId()%>" >
                <input type="submit" name="command" class ="btn" value="View films" >
                </form>
            </td>
        </tr>

        <%
            }
        %>
    </table>
<%@include file="footer.jsp" %>
        
