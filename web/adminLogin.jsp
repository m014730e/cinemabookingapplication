<%@page import="beans.Admin" %>
<%@include file="header.jsp" %>
<jsp:useBean id="admin" class="Admin" scope="session" />

<h2>Administrator login</h2>
<form action="Controller" method="post">
    <p>Username: 
        <input name="adminId"
               type="text"
               class ="btn"
               required/>
    </p>
    <p>Password: 
        <input name="password" 
               type="password"
               class ="btn"
               required/>
    </p>
    
    <p>
        <input type="submit" class ="btn" name="command" value="Admin log-in" />
        <input type="reset" class ="btn" value="Reset" />
</p>
</form>
    <%@include file="footer.jsp" %>