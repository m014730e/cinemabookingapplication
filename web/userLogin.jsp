<%@page import="beans.User" %>
<%@include file="header.jsp" %>
<jsp:useBean id="user" class="User" scope="session" />

<h2>User login</h2>
<form action="Controller" method="post">
    <p>Username: 
        <input name="userId"
               type="text"
               class ="btn"
               required/>
    </p>
    <p>Password: 
        <input name="password" 
               type="password"
               class ="btn"
               required />
    </p>
    
    <p>
        <input type="submit" class ="btn" name="command" value="User log-in" />
        <input type="reset" class ="btn" value="Reset" />
</p>
</form>
    <%@include file="footer.jsp" %>