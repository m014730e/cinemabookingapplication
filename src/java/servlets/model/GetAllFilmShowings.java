package servlets.model;

import beans.*;
import database.table_gateway.*;
import java.io.*;
import java.util.ArrayList;
import javax.servlet.*;
import javax.servlet.http.*;
import servlets.Controller;

public class GetAllFilmShowings extends Controller
{
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException
    {   
        
        String filmTitle = (String)request.getSession().getAttribute("filmTitle");
        String cinemaId = (String)request.getSession().getAttribute("cinemaId");
        CinemaHandler cinemaHandler = new CinemaHandler();
        
        ArrayList<Showing> showingRecords = cinemaHandler.findAllShowings(filmTitle, cinemaId);
        if (!showingRecords.isEmpty())
        {
            request.getSession().setAttribute("showingsList", showingRecords);
        }
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException
    {
        processRequest(request, response);
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException
    {
        processRequest(request, response);
    }
}
