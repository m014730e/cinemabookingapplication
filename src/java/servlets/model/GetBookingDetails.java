package servlets.model;

import beans.*;
import database.table_gateway.*;
import java.io.*;
import javax.servlet.*;
import javax.servlet.http.*;
import servlets.Controller;

public class GetBookingDetails extends Controller
{
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException
    {   
        String book = (String)request.getSession().getAttribute("bookingId");
        BookingHandler bookingHandler = new BookingHandler();
         
        int bookingId = Integer.parseInt(book);
        Booking bookingRecord = bookingHandler.findBooking(bookingId);
        
        if (bookingRecord != null)
        {            
            request.setAttribute("booking", bookingRecord);            
        }
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException
    {
        processRequest(request, response);
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException
    {
        processRequest(request, response);
    }
}
