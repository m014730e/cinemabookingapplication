package servlets.model;

import beans.*;
import database.table_gateway.*;
import java.io.*;
import java.util.ArrayList;
import javax.servlet.*;
import javax.servlet.http.*;
import servlets.Controller;

public class GetAllUsers extends Controller
{
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException
    {   
        UserHandler userHandler = new UserHandler();
        
        ArrayList<User> userRecords = userHandler.findAllUsers();
        if (!userRecords.isEmpty())
        { 
            request.setAttribute("usersList", userRecords);
        }
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException
    {
        processRequest(request, response);
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException
    {
        processRequest(request, response);
    }
}
