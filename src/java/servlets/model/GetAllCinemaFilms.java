package servlets.model;

import beans.*;
import database.table_gateway.*;
import java.io.*;
import java.util.ArrayList;
import javax.servlet.*;
import javax.servlet.http.*;
import servlets.Controller;

public class GetAllCinemaFilms extends Controller
{
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException
    {   
        String cinemaId = (String)request.getSession().getAttribute("cinemaId");
        CinemaHandler cinemaHandler = new CinemaHandler();
        
        ArrayList<Showing> showingRecords = cinemaHandler.findAllFilms(cinemaId);
        if (!showingRecords.isEmpty())
        { 
            request.setAttribute("filmsList", showingRecords);
        }
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException
    {
        processRequest(request, response);
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException
    {
        processRequest(request, response);
    }
}
