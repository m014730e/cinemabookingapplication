package servlets.model;

import beans.*;
import database.table_gateway.*;
import java.io.*;
import javax.servlet.*;
import javax.servlet.http.*;
import servlets.Controller;

public class GetAdminCinema extends Controller
{
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException
    {   
        String adminId = (String)request.getSession().getAttribute("adminId");
        CinemaHandler cinemaHandler = new CinemaHandler();
        
        Cinema cinemaRecord = cinemaHandler.findByAdminId(adminId);
        
        if (cinemaRecord != null)
        {
            request.getSession().setAttribute("adminCinema", cinemaRecord);
        }
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException
    {
        processRequest(request, response);
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException
    {
        processRequest(request, response);
    }
}
