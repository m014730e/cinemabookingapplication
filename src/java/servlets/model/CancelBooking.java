package servlets.model;

import database.table_gateway.*;
import java.io.*;
import javax.servlet.*;
import javax.servlet.http.*;
import servlets.Controller;

public class CancelBooking extends Controller
{
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException
    {
        String booking = (String)request.getSession().getAttribute("bookingId");
                
        int bookingId = Integer.parseInt(booking);
        BookingHandler bookingHandler = new BookingHandler();
        
        bookingHandler.delete(bookingId);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException
    {
        processRequest(request, response);
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException
    {
        processRequest(request, response);
    }
}
