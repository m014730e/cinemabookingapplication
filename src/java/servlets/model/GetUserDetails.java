package servlets.model;

import beans.User;
import database.table_gateway.UserHandler;
import java.io.*;
import javax.servlet.*;
import javax.servlet.http.*;
import servlets.Controller;

public class GetUserDetails extends Controller
{
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException
    {
        String userId = (String)request.getSession().getAttribute("userId");
                
        UserHandler userHandler = new UserHandler();
        
        User userRecord = userHandler.findById(userId);
        
        if (userRecord != null)
        {
            request.setAttribute("user", userRecord);
        }
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException
    {
        processRequest(request, response);
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException
    {
        processRequest(request, response);
    }
}
