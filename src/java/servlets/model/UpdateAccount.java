package servlets.model;

import database.table_gateway.UserHandler;
import java.io.*;
import javax.servlet.*;
import javax.servlet.http.*;
import servlets.Controller;

public class UpdateAccount extends Controller
{
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException
    {
        String username = (String)request.getSession().getAttribute("userId");
        String title = (String)request.getSession().getAttribute("title");
        String first_name = (String)request.getSession().getAttribute("first_name");
        String last_name = (String)request.getSession().getAttribute("last_name");
        String password = (String)request.getSession().getAttribute("password");
                
        UserHandler userHandler = new UserHandler();
        
        userHandler.updateAccount(title, first_name, last_name,password,username);        
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException
    {
        processRequest(request, response);
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException
    {
        processRequest(request, response);
    }
}
