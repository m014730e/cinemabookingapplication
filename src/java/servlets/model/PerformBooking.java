package servlets.model;

import database.table_gateway.*;
import java.io.*;
import javax.servlet.*;
import javax.servlet.http.*;

public class PerformBooking extends HttpServlet
{
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException
    {   
        String showing =(String) request.getSession().getAttribute("showingId");
        int showingId = Integer.parseInt(showing);
        String userId = (String)request.getSession().getAttribute("userId");
        
        String seats = (String)request.getSession().getAttribute("number_of_seats");
        int num_of_seats = 0;
        try
        {
            num_of_seats = Integer.parseInt(seats);
        }
        catch(NumberFormatException nfe)
        {
            throw new NumberFormatException("Invalid input. Should be a number");
        }                
                
        BookingHandler bookingHandler = new BookingHandler();
        
        bookingHandler.insert(showingId, userId, num_of_seats);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException
    {
        processRequest(request, response);
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException
    {
        processRequest(request, response);
    }
}
