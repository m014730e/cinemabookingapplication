package servlets.model;

import database.table_gateway.UserHandler;
import java.io.*;
import javax.servlet.*;
import javax.servlet.http.*;
import servlets.Controller;

public class DeleteAccount extends Controller
{
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException
    {
        String username = (String)request.getSession().getAttribute("userId");
                
        UserHandler userHandler = new UserHandler();
        
        userHandler.delete(username);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException
    {
        processRequest(request, response);
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException
    {
        processRequest(request, response);
    }
}
