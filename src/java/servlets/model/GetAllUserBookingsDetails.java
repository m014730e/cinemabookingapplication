package servlets.model;

import beans.*;
import database.table_gateway.*;
import java.io.*;
import java.util.ArrayList;
import javax.servlet.*;
import javax.servlet.http.*;
import servlets.Controller;

public class GetAllUserBookingsDetails extends Controller
{
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException
    {   
        String userId = (String)request.getSession().getAttribute("userId");
        BookingHandler bookingHandler = new BookingHandler();
        
        ArrayList<Booking> bookingRecords = bookingHandler.findAllBookingsForUser(userId);
        if (!bookingRecords.isEmpty())
        { 
            request.setAttribute("bookingsList", bookingRecords);
        }
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException
    {
        processRequest(request, response);
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException
    {
        processRequest(request, response);
    }
}
