package servlets.model;

import beans.*;
import database.table_gateway.*;
import java.io.*;
import java.util.ArrayList;
import javax.servlet.*;
import javax.servlet.http.*;
import servlets.Controller;

public class GetAllCinemas extends Controller
{
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException
    {   
        CinemaHandler cinemaHandler = new CinemaHandler();
        
        ArrayList<Cinema> cinemaRecords = cinemaHandler.findAllCinemas();
        if (!cinemaRecords.isEmpty())
        {
            request.setAttribute("cinemasList", cinemaRecords);
        }
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException
    {
        processRequest(request, response);
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException
    {
        processRequest(request, response);
    }
}
