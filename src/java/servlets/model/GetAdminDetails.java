package servlets.model;

import beans.Admin;
import database.table_gateway.*;
import java.io.*;
import javax.servlet.*;
import javax.servlet.http.*;
import servlets.Controller;

public class GetAdminDetails extends Controller
{
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException
    {
        String adminId = (String)request.getSession().getAttribute("adminId");
                
        AdminHandler adminHandler = new AdminHandler();
        
        Admin adminRecord = adminHandler.findById(adminId);
        
        if (adminRecord != null)
        {
            request.setAttribute("admin", adminRecord);
        }
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException
    {
        processRequest(request, response);
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException
    {
        processRequest(request, response);
    }
}
