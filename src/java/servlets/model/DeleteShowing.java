package servlets.model;

import database.table_gateway.*;
import java.io.*;
import javax.servlet.*;
import javax.servlet.http.*;
import servlets.Controller;

public class DeleteShowing extends Controller
{
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException
    {
        String showing = (String)request.getSession().getAttribute("showingId");
        int showingId = Integer.parseInt(showing);        
        CinemaHandler cinemaHandler = new CinemaHandler();
        
        cinemaHandler.delete(showingId);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException
    {
        processRequest(request, response);
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException
    {
        processRequest(request, response);
    }
}
