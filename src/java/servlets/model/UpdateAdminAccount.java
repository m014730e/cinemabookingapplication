package servlets.model;

import database.table_gateway.*;
import java.io.*;
import javax.servlet.*;
import javax.servlet.http.*;
import servlets.Controller;

public class UpdateAdminAccount extends Controller
{
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException
    {
        String adminId = (String)request.getSession().getAttribute("adminId");
        String title = (String)request.getSession().getAttribute("title");
        String first_name = (String)request.getSession().getAttribute("first_name");
        String last_name = (String)request.getSession().getAttribute("last_name");
        String password = (String)request.getSession().getAttribute("password");
                
        AdminHandler adminHandler = new AdminHandler();
        
        adminHandler.updateAccount(title, first_name, last_name,password,adminId);        
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException
    {
        processRequest(request, response);
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException
    {
        processRequest(request, response);
    }
}
