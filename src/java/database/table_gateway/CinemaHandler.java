package database.table_gateway;

import beans.Cinema;
import beans.Showing;
import java.sql.*;
import java.util.ArrayList;

public class CinemaHandler extends DatabaseManager
{
    private static final String FIND_ALL_CINEMAS = "SELECT * FROM Cinema";
    private static final String FIND_ALL_SHOWINGS_FOR_FILM =
            "SELECT Showing.Showing_Id, Showing.Film_Title, Showing.Showing_Date, Showing.Start_Time,"
            + " Showing.End_Time, Showing.Number_Of_Seats_Available, Screen.Screen_Number"
            + " FROM Showing INNER JOIN Screen ON Showing.Film_Title = ?" +
             " WHERE Showing.Screen_Id = Screen.Screen_Id AND Screen.Cinema_Id = ? ";    
    
    private static final String FIND_ALL_FILMS_FOR_CINEMA =
            "SELECT DISTINCT FILM_TITLE FROM Showing INNER JOIN Screen ON Showing.SCREEN_ID"
            + " = Screen.SCREEN_ID WHERE Screen.Cinema_Id = ?";
    
    private static final String FIND_BY_ADMIN_ID = "SELECT * FROM Cinema INNER JOIN "
            + "Admin ON Cinema.Cinema_Id = Admin.Cinema_Id WHERE Admin.Admin_Id = ?";
    
    private static final String DELETE_SHOWING = "DELETE FROM Showing WHERE Showing_Id = ?";
    
    public CinemaHandler()
    {
    }

    public ArrayList<Cinema> findAllCinemas()
    {
        ArrayList<Cinema> cinemaRecords = new ArrayList<>();

        try
        {
            Connection con = getDatabaseConnection();
            PreparedStatement stmt = null;
            ResultSet rs = null;
            try
            {
                stmt = con.prepareStatement(FIND_ALL_CINEMAS);
                rs = stmt.executeQuery();
                while (rs.next())
                {
                    Cinema cinemaRecord = createCinema(rs);
                    cinemaRecords.add(cinemaRecord);
                }
                         
                rs.close();
                stmt.close();
            }
            catch (SQLException sqle)
            {
                sqle.printStackTrace();
            }
            finally
            {
                closeDatabaseConnection(con);
            }
        }
        catch (Exception e)
        {
            cinemaRecords = null;
        }

        return cinemaRecords;
    }

    public ArrayList<Showing> findAllShowings(String filmTitle, String cinemaId)
    {
        ArrayList<Showing> showingRecords = new ArrayList<>();

        try
        {
            Connection con = getDatabaseConnection();
            PreparedStatement stmt = null;
            ResultSet rs = null;
            try
            {
                stmt = con.prepareStatement(FIND_ALL_SHOWINGS_FOR_FILM);
                stmt.setString(1, filmTitle);
                stmt.setString(2, cinemaId);
                rs = stmt.executeQuery();
                while (rs.next())
                {
                    Showing showingRecord = createShowing(rs);
                    showingRecords.add(showingRecord);
                }
                         
                rs.close();
                stmt.close();
            }
            catch (SQLException sqle)
            {
                sqle.printStackTrace();
            }
            finally
            {
                closeDatabaseConnection(con);
            }
        }
        catch (Exception e)
        {
            showingRecords = null;
        }

        return showingRecords;
    }
    
    public ArrayList<Showing> findAllFilms(String cinemaId)
    {
        ArrayList<Showing> showingRecords = new ArrayList<>();

        try
        {
            Connection con = getDatabaseConnection();
            PreparedStatement stmt = null;
            ResultSet rs = null;
            try
            {
                stmt = con.prepareStatement(FIND_ALL_FILMS_FOR_CINEMA);
                stmt.setString(1, cinemaId);
                rs = stmt.executeQuery();
                while (rs.next())
                {
                    Showing showingRecord = createFilm(rs);
                    showingRecords.add(showingRecord);
                }
                         
                rs.close();
                stmt.close();
            }
            catch (SQLException sqle)
            {
                sqle.printStackTrace();
            }
            finally
            {
                closeDatabaseConnection(con);
            }
        }
        catch (Exception e)
        {
            showingRecords = null;
        }

        return showingRecords;
    }

    public Cinema findByAdminId(String id)
    {
        Cinema cinemaRecord = null;

        try
        {
            Connection con = getDatabaseConnection();
            PreparedStatement stmt = null;
            ResultSet rs = null;

            try
            {
                stmt = con.prepareStatement(FIND_BY_ADMIN_ID);
                stmt.setString(1, id);
                rs = stmt.executeQuery();

                if (rs.next())
                {
                    cinemaRecord = createCinema(rs);
                }

                rs.close();
                stmt.close();
            }
            catch (SQLException sqle)
            {
                sqle.printStackTrace();
            }
            finally
            {
                closeDatabaseConnection(con);
            }
        }
        catch (Exception e)
        {
        }

        return cinemaRecord;
    }

    public boolean delete(int showingId)
    {
        boolean isDeleted = false;
        try
        {
            Connection con = getDatabaseConnection();
            PreparedStatement stmt = null;
            try
            {
                stmt = con.prepareStatement(DELETE_SHOWING);
                stmt.setInt(1, showingId);
                if(stmt.executeUpdate()>0)
                {
                    isDeleted = true;
                }
                
                stmt.close();
            }
            catch (SQLException sqle)
            {
                sqle.printStackTrace();
            }
            finally
            {
                closeDatabaseConnection(con);
            }
        }
        catch (Exception e)
        {
            return false;
        }
        return isDeleted;
    }
    
    private Cinema createCinema(ResultSet rs)
    {
        Cinema cinemaRecord = new Cinema();
        try
        {
            cinemaRecord.setCinemaId(rs.getString("Cinema_id"));
            cinemaRecord.setName(rs.getString("Name"));
        }
        catch (SQLException sqle)
        {
            cinemaRecord = null;
        }
        finally
        {
            return cinemaRecord;
        }
    }
    private Showing createFilm(ResultSet rs)
    {
        Showing showingRecord = new Showing();
        try
        {
            showingRecord.setFilm_title(rs.getString("Film_Title"));
        }
        catch (SQLException sqle)
        {
            showingRecord = null;
        }
        finally
        {
            return showingRecord;
        }
    }
    
    private Showing createShowing(ResultSet rs)
    {
        Showing showingRecord = new Showing();
        try
        {
            showingRecord.setShowing_Id(rs.getInt("Showing_Id"));
            showingRecord.setFilm_title(rs.getString("Film_Title"));
            showingRecord.setShowing_date(rs.getDate("Showing_Date"));
            showingRecord.setStart_time(rs.getTime("Start_Time"));
            showingRecord.setEnd_time(rs.getTime("End_Time"));
            showingRecord.setNumber_of_seats(rs.getInt("Number_Of_Seats_Available"));
            showingRecord.setScreen_num(rs.getInt("Screen_Number"));
        }
        catch (SQLException sqle)
        {
            showingRecord = null;
        }
        finally
        {
            return showingRecord;
        }
    }
}
