package database.table_gateway;

import beans.User;
import java.sql.*;
import java.util.ArrayList;

public class UserHandler extends DatabaseManager
{
    private static final String FIND_BY_ID_AND_PASSWORD = 
                                "SELECT * FROM Cinema_User WHERE User_Id = ? "
                                + "AND Password = ?  ";
    private static final String FIND_BY_ID = "SELECT * FROM Cinema_User WHERE User_Id = ?";
    private static final String REGISTER_USER = "INSERT INTO Cinema_User(User_Id, Title,"
                                + " First_Name, Last_Name, Password) VALUES (?,?,?,?,?)";
    private static final String DELETE_USER = "DELETE FROM Cinema_User WHERE User_Id = ?";
    private static final String FIND_ALL_USERS = "SELECT * FROM Cinema_User";
    private static final String UPDATE_USER = "UPDATE Cinema_User SET Title = ?, "
                                + "First_Name = ?, Last_Name = ?, Password = ? WHERE User_Id = ?";
    
    public UserHandler()
    {
    }

    public ArrayList<User> findAllUsers()
    {
        ArrayList<User> userRecords = new ArrayList<>();

        try
        {
            Connection con = getDatabaseConnection();
            PreparedStatement stmt = null;
            ResultSet rs = null;
            try
            {
                stmt = con.prepareStatement(FIND_ALL_USERS);
                rs = stmt.executeQuery();
                while (rs.next())
                {
                    User userRecord = createUser(rs);
                    userRecords.add(userRecord);
                }
                         
                rs.close();
                stmt.close();
            }
            catch (SQLException sqle)
            {
                sqle.printStackTrace();
            }
            finally
            {
                closeDatabaseConnection(con);
            }
        }
        catch (Exception e)
        {
            userRecords = null;
        }

        return userRecords;
    }

    public User findByIdAndPassword(String id, String password )
    {
        User userRecord = null;
        try
        {
            Connection con = getDatabaseConnection();
            PreparedStatement stmt = null;
            ResultSet rs = null;

            try
            {
                stmt = con.prepareStatement(FIND_BY_ID_AND_PASSWORD);
                stmt.setString(1, id);
                stmt.setString(2, password);
                rs = stmt.executeQuery();

                if (rs.next())
                {
                    userRecord = createUser(rs);
                }

                rs.close();
                stmt.close();
            }
            catch (SQLException sqle)
            {
                sqle.printStackTrace();
            }
            finally
            {
                closeDatabaseConnection(con);
            }
        }
        catch (Exception e)
        {
        }

        return userRecord;
    }
        
    public User findById(String id)
    {
        User userRecord = null;

        try
        {
            Connection con = getDatabaseConnection();
            PreparedStatement stmt = null;
            ResultSet rs = null;

            try
            {
                stmt = con.prepareStatement(FIND_BY_ID);
                stmt.setString(1, id);
                rs = stmt.executeQuery();

                if (rs.next())
                {
                    userRecord = createUser(rs);
                }

                rs.close();
                stmt.close();
            }
            catch (SQLException sqle)
            {
                sqle.printStackTrace();
            }
            finally
            {
                closeDatabaseConnection(con);
            }
        }
        catch (Exception e)
        {
        }

        return userRecord;
    }
    public boolean registerAccount(String id, String title, String first_name, String last_name, String password )
    {
        boolean isRegistered = false;
        try
        {
            Connection con = getDatabaseConnection();
            PreparedStatement stmt = null;
            
            try
            {
                if (findById(id)==null)
                {
                    stmt = con.prepareStatement(REGISTER_USER);
                    stmt.setString(1, id);
                    stmt.setString(2, title);
                    stmt.setString(3, first_name);
                    stmt.setString(4, last_name);
                    stmt.setString(5, password);
                    if(stmt.executeUpdate() > 0)
                    {
                        isRegistered = true;
                    }

                    stmt.close();
                }
                                
            }
            catch (SQLException sqle)
            {
                sqle.printStackTrace();
            }
            finally
            {
                closeDatabaseConnection(con);
            }
        }
        catch (Exception e)
        {
            isRegistered = false;
        }
        return isRegistered;
    }

    public boolean updateAccount(String title, String first_name, String last_name, String password, String id)
    {
        boolean isUpdated = false;
        try
        {
            Connection con = getDatabaseConnection();
            PreparedStatement stmt = null;
            try
            {
                stmt = con.prepareStatement(UPDATE_USER);
                stmt.setString(1, title);
                stmt.setString(2, first_name);
                stmt.setString(3, last_name);
                stmt.setString(4, password);
                stmt.setString(5, id);
                if (stmt.executeUpdate()>0)
                {
                    isUpdated = true;                   
                }
                
                stmt.close(); 

            }
            catch (SQLException sqle)
            {
                sqle.printStackTrace();
            }
            finally
            {
                closeDatabaseConnection(con);
            }
        }
        catch (Exception e)
        {
            return false;
        }
        return isUpdated;
    }
    public boolean delete(String userId)
    {
        boolean isDeleted = false;
        try
        {
            Connection con = getDatabaseConnection();
            PreparedStatement stmt = null;
            try
            {
                stmt = con.prepareStatement(DELETE_USER);
                stmt.setString(1, userId);
                if(stmt.executeUpdate()>0)
                {
                    isDeleted = true;
                }
                
                stmt.close();
            }
            catch (SQLException sqle)
            {
                sqle.printStackTrace();
            }
            finally
            {
                closeDatabaseConnection(con);
            }
        }
        catch (Exception e)
        {
            return false;
        }
        return isDeleted;
    }

    private User createUser(ResultSet rs)
    {
        User userRecord = new User();
        try
        {
            userRecord.setUserId(rs.getString("User_Id"));
            userRecord.setTitle(rs.getString("Title"));
            userRecord.setFirstName(rs.getString("First_Name"));
            userRecord.setLastName(rs.getString("Last_Name"));
            userRecord.setPassword(rs.getString("Password"));
        }
        catch (SQLException sqle)
        {
            userRecord = null;
        }
        finally
        {
            return userRecord;
        }
    }
}
