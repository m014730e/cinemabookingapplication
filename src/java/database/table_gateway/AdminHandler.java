package database.table_gateway;

import beans.Admin;
import java.sql.*;

public class AdminHandler extends DatabaseManager
{
    private static final String FIND_BY_ID_AND_PASSWORD = 
                                "SELECT * FROM Admin WHERE Admin_Id = ? "
                                + "AND Password = ?  ";
    private static final String FIND_BY_ID = "SELECT * FROM Admin WHERE Admin_Id = ?";
    private static final String UPDATE_ADMIN ="UPDATE Admin"
            + " SET Title = ?, First_Name = ?, Last_Name = ?, Password = ? WHERE Admin_Id = ?";
    
    public AdminHandler()
    {
    }

    public Admin findByIdAndPassword(String id, String password )
    {
        Admin adminRecord = null;

        try
        {
            Connection con = getDatabaseConnection();
            PreparedStatement stmt = null;
            ResultSet rs = null;

            try
            {
                stmt = con.prepareStatement(FIND_BY_ID_AND_PASSWORD);
                stmt.setString(1, id);
                stmt.setString(2, password);
                rs = stmt.executeQuery();

                if (rs.next())
                {
                    adminRecord = createAdmin(rs);
                }

                rs.close();
                stmt.close();
            }
            catch (SQLException sqle)
            {
                sqle.printStackTrace();
            }
            finally
            {
                closeDatabaseConnection(con);
            }
        }
        catch (Exception e)
        {
        }

        return adminRecord;
    }
      
    public Admin findById(String id)
    {
        Admin adminRecord = null;

        try
        {
            Connection con = getDatabaseConnection();
            PreparedStatement stmt = null;
            ResultSet rs = null;

            try
            {
                stmt = con.prepareStatement(FIND_BY_ID);
                stmt.setString(1, id);
                rs = stmt.executeQuery();

                if (rs.next())
                {
                    adminRecord = createAdmin(rs);
                }

                rs.close();
                stmt.close();
            }
            catch (SQLException sqle)
            {
                sqle.printStackTrace();
            }
            finally
            {
                closeDatabaseConnection(con);
            }
        }
        catch (Exception e)
        {
        }

        return adminRecord;
    }
    public boolean updateAccount(String title, String first_name, String last_name, String password, String id)
    {
        boolean isUpdated = false;
        try
        {
            Connection con = getDatabaseConnection();
            PreparedStatement stmt = null;

            try
            {
                stmt = con.prepareStatement(UPDATE_ADMIN);
                stmt.setString(1, title);
                stmt.setString(2, first_name);
                stmt.setString(3, last_name);
                stmt.setString(4, password);
                stmt.setString(5, id);
                if(stmt.executeUpdate()>0)
                {
                    isUpdated = true;
                }

                stmt.close();
            }
            catch (SQLException sqle)
            {
                sqle.printStackTrace();
            }
            finally
            {
                closeDatabaseConnection(con);
            }
        }
        catch (Exception e)
        {
            return false;
        }
        return isUpdated;
    }
    private Admin createAdmin(ResultSet rs)
    {
        Admin adminRecord = new Admin();
        try
        {
            adminRecord.setAdminId(rs.getString("Admin_Id"));
            adminRecord.setTitle(rs.getString("Title"));
            adminRecord.setFirstName(rs.getString("First_Name"));
            adminRecord.setLastName(rs.getString("Last_Name"));
            adminRecord.setPassword(rs.getString("Password"));
            adminRecord.setCinemaId(rs.getString("Cinema_Id"));
        }
        catch (SQLException sqle)
        {
            adminRecord = null;
        }
        finally
        {
            return adminRecord;
        }
    }
}
