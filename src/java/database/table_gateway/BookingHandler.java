package database.table_gateway;

import beans.Booking;
import java.sql.*;
import java.util.ArrayList;

public class BookingHandler extends DatabaseManager 
{
    private static final String DELETE_BOOKING = "DELETE FROM Booking WHERE Booking_Id = ?";    
    private static final String INSERT_BOOKING = "INSERT INTO Booking(Number_Of_Seats, User_Id, Showing_Id) VALUES (?,?,?)";
    private static final String FIND_ALL_BOOKINGS_FOR_USER = "SELECT * FROM Booking WHERE User_Id = ?";
    private static final String FIND_BOOKING = "SELECT BOOKING.BOOKING_ID, BOOKING.NUMBER_OF_SEATS, "
            +   "SHOWING.FILM_TITLE, SHOWING.SHOWING_DATE, SHOWING.START_TIME, SHOWING.END_TIME, " +
                "SCREEN.SCREEN_NUMBER, CINEMA.NAME  FROM BOOKING INNER JOIN SHOWING ON " +
                "BOOKING.SHOWING_ID = SHOWING.SHOWING_ID INNER JOIN SCREEN ON " +
                "SHOWING.SCREEN_ID = SCREEN.SCREEN_ID INNER JOIN CINEMA ON " +
                "SCREEN.CINEMA_ID = CINEMA.CINEMA_ID " +
                "WHERE BOOKING.BOOKING_ID = ?";
    
    private static final String UPDATE_AVAILABLE_SEATS= 
                "UPDATE Showing SET Number_Of_Seats_Available"
                + " = (Number_Of_Seats_Available - ?) "
                + "WHERE Showing_Id = ?";
    
    public BookingHandler()
    {
    }

    public ArrayList<Booking> findAllBookingsForUser(String id)
    {
        ArrayList<Booking> bookingRecords = new ArrayList<>();

       try
        {
            Connection con = getDatabaseConnection();
            PreparedStatement stmt = null;
            ResultSet rs = null;
            try
            {
                stmt = con.prepareStatement(FIND_ALL_BOOKINGS_FOR_USER);
                stmt.setString(1, id);
                rs = stmt.executeQuery();
                while (rs.next())
                {
                    Booking bookingRecord = createBooking(rs);
                    bookingRecords.add(bookingRecord);
                }
                         
                rs.close();
                stmt.close();
            }
            catch (SQLException sqle)
            {
                sqle.printStackTrace();
            }
            finally
            {
                closeDatabaseConnection(con);
            }
        }
        catch (Exception e)
        {
            bookingRecords = null;
        }

        return bookingRecords;
    }
    public Booking findBooking(int id)
    {
        Booking bookingRecord = null;

        try
        {
            Connection con = getDatabaseConnection();
            PreparedStatement stmt = null;
            ResultSet rs = null;

            try
            {
                stmt = con.prepareStatement(FIND_BOOKING);
                stmt.setInt(1, id);
                rs = stmt.executeQuery();

                if (rs.next())
                {
                    bookingRecord = createBookingDetails(rs);
                }

                rs.close();
                stmt.close();
            }
            catch (SQLException sqle)
            {
                sqle.printStackTrace();
            }
            finally
            {
                closeDatabaseConnection(con);
            }
        }
        catch (Exception e)
        {
        }

        return bookingRecord;
    }
    public boolean insert(int showingId, String userId, int num_of_seats) 
    {
        boolean isInserted = false;
        try
        {
            Connection con = getDatabaseConnection();
            PreparedStatement stmt = null;
            try
            {
                stmt = con.prepareStatement(INSERT_BOOKING);
                stmt.setInt(1, num_of_seats);
                stmt.setString(2, userId);
                stmt.setInt(3, showingId);
                if(stmt.executeUpdate() > 0)
                {
                    isInserted = true;
                }
                updateSeats(showingId, num_of_seats);
                stmt.close(); 
            }
            catch (SQLException sqle)
            {
                sqle.printStackTrace();
            }
            finally
            {
                closeDatabaseConnection(con);
            }
        }
        catch (Exception e)
        {
            isInserted = false;
        }

        return isInserted;
    }
    
    public boolean delete(int id)
    {
        boolean isDeleted = false;
        try
        {
            Connection con = getDatabaseConnection();
            PreparedStatement stmt = null;

            try
            {
                stmt = con.prepareStatement(DELETE_BOOKING);
                stmt.setInt(1, id);
                if(stmt.executeUpdate()>0)
                {
                    isDeleted = true;
                }
                                
                stmt.close();                
            }
            catch (SQLException sqle)
            {
                sqle.printStackTrace();
                return false;
            }
            finally
            {
                closeDatabaseConnection(con);
            }
        }
        catch(Exception e)
        {
            isDeleted = false;
        }
        return isDeleted;
    }

    public boolean updateSeats(int showingId, int num_of_seats)
    {
        boolean isUpdated = false;
        try
        {
            Connection con = getDatabaseConnection();
            PreparedStatement stmt = null;
            try
            {         
                stmt = con.prepareStatement(UPDATE_AVAILABLE_SEATS);
                stmt.setInt(1, num_of_seats);
                stmt.setInt(2, showingId);
                if(stmt.executeUpdate()>0)
                {
                    isUpdated = true;
                }
                                
                stmt.close();                
            }
            catch (SQLException sqle)
            {
                sqle.printStackTrace();
                return false;
            }
            finally
            {
                closeDatabaseConnection(con);
            }
        }
        catch(Exception e)
        {
            isUpdated = false;
        }
        return isUpdated;
    }
    
    private Booking createBooking(ResultSet rs)
    {
        Booking bookingRecord = new Booking();
        try
        {
            bookingRecord.setBookingId(rs.getInt("Booking_Id"));
            bookingRecord.setNumberOfSeats(rs.getInt("Number_Of_Seats"));
            bookingRecord.setUserId(rs.getString("User_Id"));
        }
        catch (SQLException sqle)
        {
            bookingRecord = null;
        }
        finally
        {
            return bookingRecord;
        }
    }
    private Booking createBookingDetails(ResultSet rs)
    {
        Booking bookingRecord = new Booking();
        try
        {
            bookingRecord.setBookingId(rs.getInt("Booking_Id"));
            bookingRecord.setNumberOfSeats(rs.getInt("Number_Of_Seats"));
            bookingRecord.setFilm_title(rs.getString("Film_Title"));
            bookingRecord.setShowing_date(rs.getDate("Showing_Date"));
            bookingRecord.setStart_time(rs.getTime("Start_Time"));
            bookingRecord.setEnd_time(rs.getTime("End_Time"));
            bookingRecord.setScreen_number(rs.getInt("Screen_Number"));
            bookingRecord.setCinema_name(rs.getString("Name"));
        }
        catch (SQLException sqle)
        {
            bookingRecord = null;
        }
        finally
        {
            return bookingRecord;
        }
    }
}
