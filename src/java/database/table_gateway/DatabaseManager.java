 package database.table_gateway;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class DatabaseManager
{
    private static final String DATABASE_URL = "jdbc:derby://localhost:1527/CinemaApplication;user=m014730e;password=password";

    protected Connection getDatabaseConnection() throws Exception
    {
        Connection con = null;

        try
        {
            DriverManager.registerDriver(new org.apache.derby.jdbc.ClientDriver());
            con = DriverManager.getConnection(DATABASE_URL);
        }
        catch (SQLException sqle)
        {
            String msg = "Cannot establish a connection to the database";
            throw new Exception(msg, sqle);
        }
        finally
        {
            return con;
        }
    }

    protected void closeDatabaseConnection(Connection con) throws Exception
    {
        try
        {
            con.close();
        }
        catch (SQLException sqle)
        {
            String msg = "Problem closing the connection to the database";
            throw new Exception(msg, sqle);
        }
    }
}
