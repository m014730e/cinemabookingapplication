package converters;

import javax.servlet.http.*;

public interface Converter
{
    String getAsString(HttpServletRequest request, Object value) throws Exception;
}
