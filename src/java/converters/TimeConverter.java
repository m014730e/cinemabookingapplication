package converters;

import java.sql.Time;
import java.text.SimpleDateFormat;
import javax.servlet.http.HttpServletRequest;

public class TimeConverter implements Converter
{

    private final static SimpleDateFormat sdf = new SimpleDateFormat("HH:mm");

    public TimeConverter()
    {
        sdf.setLenient(false);
    }

    @Override
    public String getAsString(HttpServletRequest request, Object value) throws Exception
    {
        if (value == null)
        {
            return "";
        }

        if (value instanceof Time)
        {
            return sdf.format((Time) value);
        }

        throw new Exception();
    }
}
