package converters;

import java.sql.Date;
import java.text.SimpleDateFormat;
import javax.servlet.http.HttpServletRequest;

public class DateConverter implements Converter
{
    private final static SimpleDateFormat sdf = new SimpleDateFormat("dd-MMM-yyyy");

    public DateConverter()
    {
        sdf.setLenient(false);
    }
   
    @Override
    public String getAsString(HttpServletRequest request, Object value) throws Exception
    {
        if (value == null)
        {
            return "";
        }

        if (value instanceof Date)
        {
            return sdf.format((Date) value);
        }

        throw new Exception();
    }
}
