package commands;

import java.io.*;
import javax.servlet.*;
import javax.servlet.http.*;

public class PerformBookingCommand implements Command
{
    private HttpServletRequest  request;
    private HttpServletResponse response;
    
    public PerformBookingCommand(HttpServletRequest request, HttpServletResponse response)
    {
       this.request = request;
       this.response = response;
    }
    public String execute() throws ServletException, IOException
    {
        String viewName = "";
        try
        {
            request.getSession().getAttribute("showingId");
            request.getSession().setAttribute("number_of_seats",request.getParameter("number_of_seats"));
            request.getSession().getAttribute("userId");
           
            RequestDispatcher rd =
                              request.getRequestDispatcher("/performBooking");
            rd.include(request, response);

            viewName = "viewBookingSuccessful.jsp";
        }
        catch (NumberFormatException e)
        {
            viewName = "inputError.jsp";
        }
        finally
        {
            return viewName;
        }
    }
}
