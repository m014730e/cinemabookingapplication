package commands;

import beans.Admin;
import java.io.*;
import javax.servlet.*;
import javax.servlet.http.*;

public class AdminLoginCommand implements Command
{
    private HttpServletRequest  request;
    private HttpServletResponse response;
    
    public AdminLoginCommand(HttpServletRequest request, HttpServletResponse response)
    {
       this.request = request;
       this.response = response;
    }
    public String execute() throws ServletException, IOException
    {
        request.getSession().setAttribute("adminId", request.getParameter("adminId"));
        request.getSession().setAttribute("password", request.getParameter("password"));

        String viewName = "";
        RequestDispatcher rd =
                          request.getRequestDispatcher("/getAdminLoginDetails");
        rd.include(request, response);

        Admin admin = (Admin) request.getAttribute("admin");
        if (admin == null)
        {
            viewName = "viewLogInError.jsp";
        }

        else
        {
            viewName = "viewAdminDetails.jsp";
        }
        
        return viewName;
    }
}
