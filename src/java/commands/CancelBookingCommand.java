package commands;

import java.io.*;
import javax.servlet.*;
import javax.servlet.http.*;

public class CancelBookingCommand implements Command
{
    private HttpServletRequest  request;
    private HttpServletResponse response;
    
    public CancelBookingCommand(HttpServletRequest request, HttpServletResponse response)
    {
       this.request = request;
       this.response = response;
    }
    public String execute() throws ServletException, IOException
    {
        request.getSession().setAttribute("bookingId", request.getParameter("bookingId"));

        String viewName = "";
        RequestDispatcher rd =
                          request.getRequestDispatcher("/cancelBooking");
        rd.include(request, response);

        viewName = "viewBookingCancelled.jsp";
        return viewName;
    }
}
