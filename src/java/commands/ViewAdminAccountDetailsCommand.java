package commands;

import beans.User;
import java.io.*;
import javax.servlet.*;
import javax.servlet.http.*;

public class ViewAdminAccountDetailsCommand implements Command
{
    private HttpServletRequest  request;
    private HttpServletResponse response;
    
    public ViewAdminAccountDetailsCommand(HttpServletRequest request, HttpServletResponse response)
    {
       this.request = request;
       this.response = response;
    }
    public String execute() throws ServletException, IOException
    {
        request.getSession().setAttribute("adminId", request.getParameter("adminId"));
        
        String viewName = "";
        RequestDispatcher rd =
                          request.getRequestDispatcher("/getAdminDetails");
        rd.include(request, response);
                
        viewName = "viewAdminDetails.jsp";        
        
        return viewName;
    }
}
