package commands;

import beans.User;
import java.io.*;
import java.util.ArrayList;
import javax.servlet.*;
import javax.servlet.http.*;

public class UserLoginCommand implements Command
{
    private HttpServletRequest  request;
    private HttpServletResponse response;
    
    public UserLoginCommand(HttpServletRequest request, HttpServletResponse response)
    {
       this.request = request;
       this.response = response;
    }
    public String execute() throws ServletException, IOException
    {
        request.getSession().setAttribute("userId", request.getParameter("userId"));
        
        request.getSession().setAttribute("password", request.getParameter("password"));

        String viewName = "";
        RequestDispatcher rd =
                          request.getRequestDispatcher("/getUserLoginDetails");
        rd.include(request, response);

        User user = (User) request.getAttribute("user");
        if (user == null)
        {
            viewName = "viewLogInError.jsp";
        }

        else
        {
            viewName = "viewUserDetails.jsp";
        }
        
        return viewName;
    }
}
