package commands;

import beans.Showing;
import java.io.*;
import java.util.ArrayList;
import javax.servlet.*;
import javax.servlet.http.*;

public class ViewAdminFilmsCommand implements Command
{
    private HttpServletRequest  request;
    private HttpServletResponse response;
    
    public ViewAdminFilmsCommand(HttpServletRequest request, HttpServletResponse response)
    {
       this.request = request;
       this.response = response;
    }    
    public String execute() throws ServletException, IOException
    {        
        request.getSession().setAttribute("cinemaId", request.getParameter("cinemaId"));
        String viewName = "";
        RequestDispatcher rd;

        rd = request.getRequestDispatcher("/getAllCinemaFilms");
        rd.include(request, response);

        ArrayList<Showing> showings =
                            (ArrayList<Showing>) request.getAttribute("filmsList");
        if (showings == null)
        {
            throw new ServletException("Films list not available");
        }
        viewName = "viewAdminCinemaFilms.jsp";
        return viewName;
    }
}
