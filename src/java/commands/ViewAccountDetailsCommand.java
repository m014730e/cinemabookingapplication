package commands;

import beans.User;
import java.io.*;
import javax.servlet.*;
import javax.servlet.http.*;

public class ViewAccountDetailsCommand implements Command
{
    private HttpServletRequest  request;
    private HttpServletResponse response;
    
    public ViewAccountDetailsCommand(HttpServletRequest request, HttpServletResponse response)
    {
       this.request = request;
       this.response = response;
    }
    public String execute() throws ServletException, IOException
    {
        request.getSession().setAttribute("userId", request.getParameter("userId"));
        
        String viewName = "";
        RequestDispatcher rd =
                          request.getRequestDispatcher("/getUserDetails");
        rd.include(request, response);

        User user = (User) request.getAttribute("user");
        if (user == null)
        {
            viewName = "viewLogInError.jsp";
        }

        else
        {
            viewName = "viewUserDetails.jsp";
        }
        
        return viewName;
    }
}
