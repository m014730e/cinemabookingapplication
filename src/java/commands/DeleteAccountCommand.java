package commands;

import java.io.*;
import javax.servlet.*;
import javax.servlet.http.*;

public class DeleteAccountCommand implements Command
{
    private HttpServletRequest  request;
    private HttpServletResponse response;
    
    public DeleteAccountCommand(HttpServletRequest request, HttpServletResponse response)
    {
       this.request = request;
       this.response = response;
    }
    public String execute() throws ServletException, IOException
    {
        request.getSession().setAttribute("userId", request.getParameter("userId"));

        String viewName = "";
        RequestDispatcher rd =
                          request.getRequestDispatcher("/deleteAccount");
        rd.include(request, response);

        viewName = "viewAccountDeleted.jsp";
        return viewName;
    }
}
