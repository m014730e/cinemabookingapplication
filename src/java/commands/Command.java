package commands;
import java.io.*;
import javax.servlet.*;

public interface Command
{
    String execute() throws ServletException, IOException;
}
