package commands;

import beans.Showing;
import java.io.*;
import java.util.ArrayList;
import javax.servlet.*;
import javax.servlet.http.*;

public class ViewAdminShowingsCommand implements Command
{
    private HttpServletRequest  request;
    private HttpServletResponse response;
    
    public ViewAdminShowingsCommand(HttpServletRequest request, HttpServletResponse response)
    {
       this.request = request;
       this.response = response;
    }    
    public String execute() throws ServletException, IOException
    {      
        request.getSession().setAttribute("filmTitle", request.getParameter("filmTitle"));
        String viewName = "";
        RequestDispatcher rd;

        rd = request.getRequestDispatcher("/getAllFilmShowings");
        rd.include(request, response);

        ArrayList<Showing> showings =
                            (ArrayList<Showing>) request.getSession().getAttribute("showingsList");
        if (showings == null)
        {
            viewName = "noAdminShowings.jsp";
        }
        else
        {
            viewName = "viewAdminShowings.jsp";
        }
        return viewName;
    }
}
