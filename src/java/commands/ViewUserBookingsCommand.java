package commands;

import beans.Booking;
import beans.User;
import java.io.*;
import java.util.ArrayList;
import javax.servlet.*;
import javax.servlet.http.*;

public class ViewUserBookingsCommand implements Command
{
    private HttpServletRequest  request;
    private HttpServletResponse response;
    
    public ViewUserBookingsCommand(HttpServletRequest request, HttpServletResponse response)
    {
       this.request = request;
       this.response = response;
    }    
    public String execute() throws ServletException, IOException
    {
        request.getSession().setAttribute("userId", request.getParameter("userId"));
        
        String viewName = "";
        RequestDispatcher rd;

        rd = request.getRequestDispatcher("/getAllUserBookingsDetails");
        rd.include(request, response);

        ArrayList<Booking> bookings =
                            (ArrayList<Booking>) request.getAttribute("bookingsList");
        if (bookings == null)
        {
            throw new ServletException("Bookings list not available");
        }
        viewName = "viewAllUserBookingsDetails.jsp";
        return viewName;
    }
}
