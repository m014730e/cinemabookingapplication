package commands;

import beans.User;
import java.io.*;
import java.util.ArrayList;
import javax.servlet.*;
import javax.servlet.http.*;

public class ViewAllUsersCommand implements Command
{
    private HttpServletRequest  request;
    private HttpServletResponse response;
    
    public ViewAllUsersCommand(HttpServletRequest request, HttpServletResponse response)
    {
       this.request = request;
       this.response = response;
    }    
    public String execute() throws ServletException, IOException
    {        
        String viewName = "";
        RequestDispatcher rd;

        rd = request.getRequestDispatcher("/getAllUsers");
        rd.include(request, response);

        ArrayList<User> users =
                            (ArrayList<User>) request.getAttribute("usersList");
        if (users == null)
        {
            throw new ServletException("User list not available");
        }
        viewName = "viewAllUsers.jsp";
        return viewName;
    }
}
