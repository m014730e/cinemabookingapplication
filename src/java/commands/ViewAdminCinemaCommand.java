package commands;

import beans.Cinema;
import java.io.*;
import java.util.ArrayList;
import javax.servlet.*;
import javax.servlet.http.*;

public class ViewAdminCinemaCommand implements Command
{
    private HttpServletRequest  request;
    private HttpServletResponse response;
    
    public ViewAdminCinemaCommand(HttpServletRequest request, HttpServletResponse response)
    {
       this.request = request;
       this.response = response;
    }    
    public String execute() throws ServletException, IOException
    {        
        request.getSession().setAttribute("adminId", request.getParameter("adminId"));
        
        String viewName = "";
        RequestDispatcher rd;

        rd = request.getRequestDispatcher("/getAdminCinema");
        rd.include(request, response);

        Cinema cinema = (Cinema) request.getSession().getAttribute("adminCinema");
        
        if (cinema == null)
        {
            throw new ServletException("Admin cinema not available");
        }
        viewName = "viewAdminCinema.jsp";
        return viewName;
    }
}
