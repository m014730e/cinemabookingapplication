package commands;

import java.io.*;
import javax.servlet.*;
import javax.servlet.http.*;

public class DeleteShowingCommand implements Command
{
    private HttpServletRequest  request;
    private HttpServletResponse response;
    
    public DeleteShowingCommand(HttpServletRequest request, HttpServletResponse response)
    {
       this.request = request;
       this.response = response;
    }
    public String execute() throws ServletException, IOException
    {
        request.getSession().setAttribute("showingId", request.getParameter("showingId"));

        String viewName = "";
        RequestDispatcher rd =
                          request.getRequestDispatcher("/deleteShowing");
        rd.include(request, response);

        viewName = "viewShowingDeleted.jsp";
        return viewName;
    }
}
