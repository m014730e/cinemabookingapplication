package commands;

import beans.Booking;
import java.io.*;
import javax.servlet.*;
import javax.servlet.http.*;

public class ViewBookingDetailsCommand implements Command
{
    private HttpServletRequest  request;
    private HttpServletResponse response;
    
    public ViewBookingDetailsCommand(HttpServletRequest request, HttpServletResponse response)
    {
       this.request = request;
       this.response = response;
    }    
    public String execute() throws ServletException, IOException
    {
        request.getSession().setAttribute("bookingId", request.getParameter("bookingId"));
        
        String viewName = "";
        RequestDispatcher rd;

        rd = request.getRequestDispatcher("/getBookingDetails");
        rd.include(request, response);

        Booking booking = (Booking) request.getAttribute("booking");
        if (booking == null)
        {
            throw new ServletException("Booking not available");
        }
        viewName = "viewBookingDetails.jsp";
        return viewName;
    }
}
