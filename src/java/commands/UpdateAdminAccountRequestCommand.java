package commands;

import beans.Admin;
import java.io.*;
import javax.servlet.*;
import javax.servlet.http.*;

public class UpdateAdminAccountRequestCommand implements Command
{
    private HttpServletRequest  request;
    private HttpServletResponse response;
    
    public UpdateAdminAccountRequestCommand(HttpServletRequest request, HttpServletResponse response)
    {
       this.request = request;
       this.response = response;
    }
    public String execute() throws ServletException, IOException
    {
        request.getSession().setAttribute("adminId", request.getParameter("adminId"));

        String viewName = "";
        RequestDispatcher rd =
            request.getRequestDispatcher("/getAdminDetails");
        rd.include(request, response);

        Admin admin = (Admin) request.getAttribute("admin");
        if (admin == null)
        {
            throw new ServletException("Admin not available");
        }

        viewName = "viewAdminUpdateDetails.jsp";
        return viewName;
    }
}
