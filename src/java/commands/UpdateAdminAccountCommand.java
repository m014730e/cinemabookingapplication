package commands;

import beans.Admin;
import java.io.*;
import javax.servlet.*;
import javax.servlet.http.*;

public class UpdateAdminAccountCommand implements Command
{
    private HttpServletRequest  request;
    private HttpServletResponse response;
    
    public UpdateAdminAccountCommand(HttpServletRequest request, HttpServletResponse response)
    {
       this.request = request;
       this.response = response;
    }
    public String execute() throws ServletException, IOException
    {
        String viewName = "";
        request.getSession().setAttribute("adminId", request.getParameter("adminId"));
        request.getSession().setAttribute("title", request.getParameter("title"));
        request.getSession().setAttribute("first_name", request.getParameter("first_name"));
        request.getSession().setAttribute("last_name", request.getParameter("last_name"));
        request.getSession().setAttribute("password", request.getParameter("password"));

        RequestDispatcher rd =
                          request.getRequestDispatcher("/updateAdminAccount");
        rd.include(request, response);

        rd =
            request.getRequestDispatcher("/getAdminLoginDetails");
        rd.include(request, response);

        Admin admin = (Admin) request.getAttribute("admin");
        if (admin == null)
        {
            throw new ServletException("Admin not available");
        }

        viewName = "viewAdminDetails.jsp";

        return viewName;
    }
}
