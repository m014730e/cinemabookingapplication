package commands;

import beans.User;
import java.io.*;
import javax.servlet.*;
import javax.servlet.http.*;

public class UpdateAccountRequestCommand implements Command
{
    private HttpServletRequest  request;
    private HttpServletResponse response;
    
    public UpdateAccountRequestCommand(HttpServletRequest request, HttpServletResponse response)
    {
       this.request = request;
       this.response = response;
    }
    public String execute() throws ServletException, IOException
    {
        request.getSession().setAttribute("userId", request.getParameter("userId"));

        String viewName = "";
        RequestDispatcher rd =
            request.getRequestDispatcher("/getUserDetails");
        rd.include(request, response);

        User user = (User) request.getAttribute("user");
        if (user == null)
        {
            throw new ServletException("User not available");
        }

        viewName = "viewUserUpdateDetails.jsp";
        return viewName;
    }
}
