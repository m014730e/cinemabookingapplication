package commands;
import javax.servlet.http.*;

public abstract class CommandFactory
{
    public static Command createCommand(HttpServletRequest request, 
                                        HttpServletResponse response)
    {
        Command command = null;
        String userCommand = request.getParameter("command");
        
        if (userCommand != null && !userCommand.equals(""))
        {
            if (userCommand.equalsIgnoreCase("User log-in"))
            {
                command = new UserLoginCommand(request, response);
            }
            if (userCommand.equalsIgnoreCase("View your bookings"))
            {
                command = new ViewUserBookingsCommand(request, response);
            }
            if (userCommand.equalsIgnoreCase("View all cinemas"))
            {
                command = new ViewAllCinemasCommand(request, response);
            }
            if (userCommand.equalsIgnoreCase("View films"))
            {
                command = new ViewCinemaFilmsCommand(request, response);
            }
            if (userCommand.equalsIgnoreCase("View showings"))
            {
                command = new ViewFilmShowingsCommand(request, response);
            }
            if (userCommand.equalsIgnoreCase("Delete account"))
            {
                command = new DeleteAccountCommand(request, response);
            }
            if (userCommand.equalsIgnoreCase("Register"))
            {
                command = new RegisterAccountCommand(request, response);
            }
            if (userCommand.equalsIgnoreCase("Update account"))
            {
                command = new UpdateAccountRequestCommand(request, response);
            }
            if (userCommand.equalsIgnoreCase("Update"))
            {
                command = new UpdateAccountCommand(request, response);
            }
            if (userCommand.equalsIgnoreCase("Admin log-in"))
            {
                command = new AdminLoginCommand(request, response);
            }
            
            if (userCommand.equalsIgnoreCase("View admin's cinema"))
            {
                command = new ViewAdminCinemaCommand(request, response);
            }
            
            if (userCommand.equalsIgnoreCase("View all users"))
            {
                command = new ViewAllUsersCommand(request, response);
            }
            
            if (userCommand.equalsIgnoreCase("Update admin's account"))
            {
                command = new UpdateAdminAccountRequestCommand(request, response);
            }
            
            if (userCommand.equalsIgnoreCase("Update admin details"))
            {
                command = new UpdateAdminAccountCommand(request, response);
            }
            
            if (userCommand.equalsIgnoreCase("View booking"))
            {
                command = new ViewBookingDetailsCommand(request, response);
            }
            
            if (userCommand.equalsIgnoreCase("Cancel booking"))
            {
                command = new CancelBookingCommand(request, response);
            }
            
            if (userCommand.equalsIgnoreCase("Book"))
            {
                command = new BookTicketsCommand(request, response);
            }
            
            if (userCommand.equalsIgnoreCase("Perform Booking"))
            {
                command = new PerformBookingCommand(request, response);
            }
            
            if (userCommand.equalsIgnoreCase("View films for admin"))
            {
                command = new ViewAdminFilmsCommand(request, response);
            }
            
            if (userCommand.equalsIgnoreCase("View showings for admin"))
            {
                command = new ViewAdminShowingsCommand(request, response);
            }
            
            if (userCommand.equalsIgnoreCase("Delete showing"))
            {
                command = new DeleteShowingCommand(request, response);
            } 
            if (userCommand.equalsIgnoreCase("View account details"))
            {
                command = new ViewAccountDetailsCommand(request, response);
            }
            if (userCommand.equalsIgnoreCase("View admin's account details"))
            {
                command = new ViewAdminAccountDetailsCommand(request, response);
            }
        }
        return command;
    }
}