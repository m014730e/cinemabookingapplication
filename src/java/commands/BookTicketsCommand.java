package commands;

import java.io.*;
import javax.servlet.*;
import javax.servlet.http.*;

public class BookTicketsCommand implements Command
{
    private HttpServletRequest  request;
    private HttpServletResponse response;
    
    public BookTicketsCommand(HttpServletRequest request, HttpServletResponse response)
    {
       this.request = request;
       this.response = response;           
    }    
    public String execute() throws ServletException, IOException
    {        
        request.getSession().setAttribute("showingId", request.getParameter("showingId"));
        request.getSession().setAttribute("userId", request.getParameter("userId"));
        String viewName = "";
//
//        RequestDispatcher rd;
//        rd = request.getRequestDispatcher("/performBooking.jsp");
//        rd.include(request, response);
//        
        viewName = "enterNumberOfSeats.jsp";
        return viewName;
    }
}
