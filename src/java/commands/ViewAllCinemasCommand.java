package commands;

import beans.Cinema;
import java.io.*;
import java.util.ArrayList;
import javax.servlet.*;
import javax.servlet.http.*;

public class ViewAllCinemasCommand implements Command
{
    private HttpServletRequest  request;
    private HttpServletResponse response;
    
    public ViewAllCinemasCommand(HttpServletRequest request, HttpServletResponse response)
    {
       this.request = request;
       this.response = response;
    }    
    public String execute() throws ServletException, IOException
    {  
        String viewName = "";
        RequestDispatcher rd;

        rd = request.getRequestDispatcher("/getAllCinemas");
        rd.include(request, response);

        ArrayList<Cinema> cinemas =
                            (ArrayList<Cinema>) request.getAttribute("cinemasList");
        if (cinemas == null)
        {
            throw new ServletException("Cinema list not available");
        }
        viewName = "viewAllCinemas.jsp";
        return viewName;
    }
}
