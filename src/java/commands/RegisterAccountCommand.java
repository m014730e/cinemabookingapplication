package commands;

import beans.User;
import java.io.*;
import javax.servlet.*;
import javax.servlet.http.*;

public class RegisterAccountCommand implements Command
{
    private HttpServletRequest  request;
    private HttpServletResponse response;
    
    public RegisterAccountCommand(HttpServletRequest request, HttpServletResponse response)
    {
       this.request = request;
       this.response = response;
    }
    public String execute() throws ServletException, IOException
    {
        request.getSession().setAttribute("userId", request.getParameter("userId"));
        request.getSession().setAttribute("title", request.getParameter("title"));
        request.getSession().setAttribute("first_name", request.getParameter("first_name"));
        request.getSession().setAttribute("last_name", request.getParameter("last_name"));
        request.getSession().setAttribute("password", request.getParameter("password"));

        String viewName = "";
        RequestDispatcher rd =
                          request.getRequestDispatcher("/registerAccount");
        rd.include(request, response);
        
        rd =
            request.getRequestDispatcher("/getUserLoginDetails");
        rd.include(request, response);

        User user = (User) request.getAttribute("user");
        if (user == null)
        {
            viewName = "registerError.jsp";
        }
        else
        {
            viewName = "viewUserDetails.jsp";
        }
        return viewName;
    }
}
