 
package beans;

import java.io.Serializable;

public class Cinema implements Serializable
{
    private String cinemaId;
    private String name;
    
    public Cinema()
    {
        this.cinemaId = "";
        this.name = "";
    }

    public String getCinemaId()
    {
        return cinemaId;
    }

    public void setCinemaId(String cinemaId)
    {
        this.cinemaId = cinemaId;
    }

    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }
}
