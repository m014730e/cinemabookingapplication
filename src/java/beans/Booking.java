package beans;

import java.io.Serializable;
import java.sql.Date;
import java.sql.Time;

public class Booking implements Serializable
{
    private int bookingId;
    private int numberOfSeats;
    private String userId;
    private String film_title;
    private Date showing_date;
    private Time start_time;
    private Time end_time;
    private int screen_number;
    private String cinema_name;
    
    public Booking()
    {
        this.bookingId = 0;
        this.numberOfSeats = 0;
        this.userId = "";
    }

    public int getBookingId() 
    {
        return bookingId;
    }

    public void setBookingId(int bookingId) 
    {
        this.bookingId = bookingId;
    }

    public int getNumberOfSeats()
    {
        return numberOfSeats;
    }

    public void setNumberOfSeats(int numberOfSeats) 
    {
        this.numberOfSeats = numberOfSeats;
    }

    public String getUserId() 
    {
        return userId;
    }

    public void setUserId(String userId) 
    {
        this.userId = userId;
    }
     public String getFilm_title() 
    {
        return film_title;
    }

    public void setFilm_title(String film_title)
    {
        this.film_title = film_title;
    }

    public Date getShowing_date()
    {
        return showing_date;
    }

    public void setShowing_date(Date showing_date) 
    {
        this.showing_date = showing_date;
    }

    public Time getStart_time() 
    {
        return start_time;
    }

    public void setStart_time(Time start_time)
    {
        this.start_time = start_time;
    }

    public Time getEnd_time() 
    {
        return end_time;
    }

    public void setEnd_time(Time end_time)
    {
        this.end_time = end_time;
    }

    public int getScreen_number()
    {
        return screen_number;
    }

    public void setScreen_number(int screen_number) 
    {
        this.screen_number = screen_number;
    }

    public String getCinema_name() 
    {
        return cinema_name;
    }

    public void setCinema_name(String cinema_name) 
    {
        this.cinema_name = cinema_name;
    }
}

