package beans;

import java.io.Serializable;
import java.sql.Date;
import java.sql.Time;

public class Showing implements Serializable
{
    private int showing_Id;
    private String film_title;
    private Date  showing_date;
    private Time  start_time;
    private Time end_time;
    private int number_of_seats;
    private int screen_num;
    
    public Showing()
    {
    }

    public int getShowing_Id() {
        return showing_Id;
    }

    public void setShowing_Id(int showing_Id) 
    {
        this.showing_Id = showing_Id;
    }

    public String getFilm_title() 
    {
        return film_title;
    }

    public void setFilm_title(String film_title) 
    {
        this.film_title = film_title;
    }

    public Date getShowing_date()
    {
        return showing_date;        
    }

    public void setShowing_date(Date showing_date) 
    {
        this.showing_date = showing_date;
    }

    public Time getStart_time()
    {
        return start_time;
    }

    public void setStart_time(Time start_time)
    {
        this.start_time = start_time;
    }

    public Time getEnd_time() 
    {
        return end_time;
    }

    public void setEnd_time(Time end_time)
    {
        this.end_time = end_time;
    }

    public int getNumber_of_seats() 
    {
        return number_of_seats;
    }

    public void setNumber_of_seats(int number_of_seats)
    {
        this.number_of_seats = number_of_seats;
    }

    public int getScreen_num()
    {
        return screen_num;
    }

    public void setScreen_num(int screen_num)
    {
        this.screen_num = screen_num;
    }
}
