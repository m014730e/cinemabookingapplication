package beans;

import java.io.Serializable;

public class Admin implements Serializable
{   
    private String adminId;
    private String title;
    private String firstName;
    private String lastName;
    private String password;
    private String cinemaId;
    
    public Admin()
    {
        this.adminId = "";
        this.title = "";
        this.firstName = "";
        this.lastName = "";
        this.password = "";
        this.cinemaId = "";
    }
    public String getAdminId()
    {
        return adminId;
    }

    public void setAdminId(String adminId) 
    {
        this.adminId = adminId;
    }

    public String getTitle() 
    {
        return title;
    }

    public void setTitle(String title) 
    {
        this.title = title;
    }

    public String getFirstName() 
    {
        return firstName;
    }

    public void setFirstName(String firstName) 
    {
        this.firstName = firstName;
    }

    public String getLastName() 
    {
        return lastName;
    }

    public void setLastName(String lastName)
    {
        this.lastName = lastName;
    }

    public String getPassword() 
    {
        return password;
    }

    public void setPassword(String password)
    {
        this.password = password;
    }
    
    public String getCinemaId()
    {
        return cinemaId;
    }

    public void setCinemaId(String cinemaId) 
    {
        this.cinemaId = cinemaId;
    }
}
