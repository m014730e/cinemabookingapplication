package beans;

import java.io.Serializable;

public class User implements Serializable
{   
    private String userId;
    private String title;
    private String firstName;
    private String lastName;
    private String password;
    
    public User() 
    {
       userId = "";
       title = "";
       firstName = "";
       lastName = "";
       password = "";       
    }
    public String getUserId()
    {
        return userId;
    }

    public void setUserId(String userId) 
    {
        this.userId = userId;
    }

    public String getTitle() 
    {
        return title;
    }

    public void setTitle(String title) 
    {
        this.title = title;
    }

    public String getFirstName() 
    {
        return firstName;
    }

    public void setFirstName(String firstName) 
    {
        this.firstName = firstName;
    }

    public String getLastName() 
    {
        return lastName;
    }

    public void setLastName(String lastName)
    {
        this.lastName = lastName;
    }

    public String getPassword() 
    {
        return password;
    }

    public void setPassword(String password)
    {
        this.password = password;
    }
}
